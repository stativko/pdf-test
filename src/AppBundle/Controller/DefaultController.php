<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Validator\Constraints\File;

class DefaultController extends Controller {
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request)
	{
		$formBuilder = $this->createFormBuilder();
		$formBuilder
			->add('pdf_file', 'file', [
				'constraints' => [
					new File([
						'mimeTypes' => ['application/vnd.fdf', 'application/pdf']
					])
				]
			])
			->add('upload', 'submit');
		$form = $formBuilder->getForm();

		$form->handleRequest($request);

		$parsedFields = [];

		$filename = null;
		if ($form->isValid()) {
			$data = $form->getData();
			if (isset($data['pdf_file']) && $data['pdf_file'] instanceof UploadedFile) {
				$filename = $this->processUploadedFile($data['pdf_file'], $parsedFields);
			}
		}
		// replace this example code with whatever you need
		return $this->render('default/index.html.twig', [
			'form' => $form->createView(),
			'container' => $this->container,
			'fields' => $parsedFields,
			'filename' => $filename
		]);
	}

	/**
	 * @Route("/render-pdf/{file}", name="render_pdf")
	 */
	public function renderPdfAction($file)
	{
		return $this->get('salva_pdf_js.controller')->renderPdf('/upload/' . $file);
	}

	/**
	 * @Route("/display-form/{file}", name="display_form")
	 */
	public function displayFormAction($file)
	{
		$uploadPath = realpath($this->get('kernel')->getRootDir() . '/../web/upload');
		list($fileName, $extension) = explode('.', $file);
		$jsonString = file_get_contents($uploadPath . '/' . $fileName . '.json');
		$parseFields = [];
		array_map(function($jsonString) use (&$parseFields){
			$json = json_decode($jsonString, true);
			if ($json !== null) {
				$parseFields[] = $json;
			}
		}, explode('|~|', $jsonString));
		return $this->render("default/display_form.html.twig", [
			'fields' => $parseFields
		]);
	}

	private function processUploadedFile(UploadedFile $file, array &$parseFields)
	{
		$command = 'java -jar %s/pdf-miner.jar %s %s';
		$binPath = realpath($this->get('kernel')->getRootDir() . '/../bin/');
		$outPath = realpath($this->get('kernel')->getRootDir() . '/../web/upload');
		$fileName = uniqid();
		$commandLine = sprintf($command, $binPath, $file->getRealPath(), $outPath . '/' . $fileName . '.pdf');
		$process = new Process($commandLine);
		$process->run();
		if ($process->getExitCode() == 0) {
			// success
			$output = $process->getOutput();
			file_put_contents($outPath . '/' . $fileName. '.json', $output);
			array_map(function($jsonString) use (&$parseFields){
				$json = json_decode($jsonString, true);
				if ($json !== null) {
					$parseFields[] = $json;
				}
			}, explode('|~|', $output));
		} else {
			throw new \RuntimeException(
				sprintf("Can not parse pdf file. %s", $process->getErrorOutput())
			);
		}
		return $fileName . '.pdf';
	}
}
